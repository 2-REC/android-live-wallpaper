package com.derek.android.wallpapers.my_lwp;

import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceActivity;
import android.widget.Toast;


/**
 * Settings Activity.
 */
public class SettingsActivity extends PreferenceActivity
{
    public static final String PREF_GENERAL_ALPHA = "prefAlpha";
    public static final String PREF_GENERAL_MAX_NB = "prefMaxNb";
    public static final String PREF_GENERAL_MAX_AGE = "prefMaxAge";
    public static final String PREF_GENERAL_RESET = "prefReset";

    public static final String PREF_BG_COLOR = "prefBackgroundColor";
    public static final String PREF_BG_USE_IMAGE = "prefUseBackgroundImage";
    public static final String PREF_BG_IMAGE = "prefBackgroundImage";

    public static final String PREF_OBJECTS_SHOW_CIRCLES = "prefShowCircles";
    public static final String PREF_OBJECTS_SHOW_TRAILS = "prefShowTrails";
    public static final String PREF_OBJECTS_RADIUS_DIAGONAL = "prefRadiusDiagonal";
    public static final String PREF_OBJECTS_COLOR_DIAGONAL = "prefColorDiagonal";
    public static final String PREF_OBJECTS_RADIUS_PLUS = "prefRadiusPlus";
    public static final String PREF_OBJECTS_COLOR_PLUS = "prefColorPlus";
    public static final String PREF_OBJECTS_COLOR_FLASH = "prefColorFlash";


    @Override
    public void onBuildHeaders( List<Header> target )
    {
        loadHeadersFromResource( R.xml.prefs, target );
    }

    public void resetSettings()
    {
        SharedPreferences prefs = getSharedPreferences( LiveWallpaperImpl.SHARED_PREFS_NAME, Context.MODE_PRIVATE );
        SharedPreferences.Editor editor = prefs.edit();

        editor.remove( PREF_GENERAL_ALPHA );
        editor.remove( PREF_GENERAL_MAX_NB );
        editor.remove( PREF_GENERAL_MAX_AGE );
        editor.remove( PREF_BG_COLOR );
        editor.remove( PREF_BG_USE_IMAGE );
        editor.remove( PREF_GENERAL_RESET );
        editor.remove( PREF_BG_IMAGE );
        editor.remove( PREF_OBJECTS_SHOW_CIRCLES );
        editor.remove( PREF_OBJECTS_SHOW_TRAILS );
        editor.remove( PREF_OBJECTS_RADIUS_DIAGONAL );
        editor.remove( PREF_OBJECTS_COLOR_DIAGONAL );
        editor.remove( PREF_OBJECTS_RADIUS_PLUS );
        editor.remove( PREF_OBJECTS_COLOR_PLUS );
        editor.remove( PREF_OBJECTS_COLOR_FLASH );
        editor.commit();

        Toast.makeText( this, R.string.prefs_general_reset_notification, Toast.LENGTH_SHORT ).show();
    }

    // NEEDED FOR ANDROID >4.0!
    protected boolean isValidFragment( String fragmentName )
    {
        return SettingsFragmentGeneral.class.getName().equals( fragmentName )
                || SettingsFragmentBackground.class.getName().equals( fragmentName )
                || SettingsFragmentObjects.class.getName().equals( fragmentName );
    }

}
