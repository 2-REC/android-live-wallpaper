package com.derek.android.wallpapers.my_lwp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
//import android.graphics.RadialGradient;
//import android.graphics.Shader;


public class Element
{
    public static final int TYPE_DIAGONAL = 0;
    public static final int TYPE_PLUS = 1;

    public static final int MASK_COLOR_ALPHA = 0xFF000000;
    public static final int DEFAULT_COLOR_DIAGONAL = 0xFFFF0000;
    public static final int DEFAULT_COLOR_PLUS = 0xFF0000FF;
    public static final int DEFAULT_COLOR_FLASH_DIAGONAL = 0xFF00FF00;
    public static final int DEFAULT_COLOR_FLASH_PLUS = 0xFF0000FF;
    public static final int DEFAULT_COLOR_COLLISION_DIAGONAL = 0xFF880000;
    public static final int DEFAULT_COLOR_COLLISION_PLUS = 0xFF000088;

    public static final int DEFAULT_RADIUS = 4;


    public static final int COLLISION_NO = 0;
    private static final int COLLISION_START = 1;
    private static final int COLLISION_IN = 2;


    private static int colorDiagonal = DEFAULT_COLOR_DIAGONAL;
    private static int colorPlus = DEFAULT_COLOR_PLUS;
    private static int flashColorDiagonal = DEFAULT_COLOR_FLASH_DIAGONAL;
    private static int flashColorPlus = DEFAULT_COLOR_FLASH_PLUS;
    private static int collisionColorDiagonal = DEFAULT_COLOR_COLLISION_DIAGONAL;
    private static int collisionColorPlus = DEFAULT_COLOR_COLLISION_PLUS;


    private Set<Element> collisions = new HashSet<Element>();
    private List<FlashPoint> flashes = new ArrayList<FlashPoint>();
    private List<FlashPoint> flashesDone = new ArrayList<FlashPoint>();

    private int type;
    private int age;
    private double centerX; 
    private double centerY;
    private double velocityX;
    private double velocityY;
    private double radius;
    private double mass;
private int drawCounter;


    public Element( int type, int x, int y )
    {
        this( type, x, y, DEFAULT_RADIUS );
    }

    public Element( int type, int x, int y, int r )
    {
        this( type, x, y, r, 1 );
    }

    public Element( int type, int x, int y, int r, int m )
    {
    	this.type = type;
        age = 0;
        centerX = x;
        centerY = y;
        velocityX = 0.0;
        velocityY = 0.0;
        radius = r;
        mass = m;
drawCounter = 0;
    }

    public void setVelocity( double x, double y )
    {
        velocityX = x;
        velocityY = y;
    }

    public int getType()
    {
        return type;
    }

    public int getAge()
    {
        return age;
    }

    public double getVelocityX()
    {
        return velocityX;
    }

    public double getVelocityY()
    {
        return velocityY;
    }

    public double getRadius()
    {
        return radius;
    }

    public double getMass()
    {
        return mass;
    }

    // Returns one of { COLLISION_NO, COLLISION_START, COLLISION_IN }.
    //  COLLISION_NO    => this Element doesn't overlap input Element
    //  COLLISION_START => this Element didn't overlap input Element last time we checked, but it does now
    //  COLLISION_IN    => this Element overlaps input Element, and it did the last time we checked
    public int collide( Element other )
    {
        int state = COLLISION_NO;
        double x = ( centerX - other.centerX );
        double y = ( centerY - other.centerY );
        double otherRadius = other.getRadius();
        double distSquared = ( x * x ) + ( y * y );
        double distRadiusSquared = ( radius + otherRadius ) * ( radius + otherRadius );

        if ( distSquared <= distRadiusSquared ) // collision
        {
            if ( collisions.add( other ) )
            {
                state = COLLISION_START;
                other.addCollision( this );
                if ( type == TYPE_DIAGONAL )
                {
                    flashes.add( new FlashPoint( this, collisionColorDiagonal, flashColorDiagonal ) );
                }
                else
                {
                    flashes.add( new FlashPoint( this, collisionColorPlus, flashColorPlus ) );
                }
                other.addFlash();

                double otherVelocityX = other.getVelocityX();
                double otherVelocityY = other.getVelocityY();
                double otherMass = other.getMass();

                final double sum = mass + otherMass;
                double tmpX0 = ( ( velocityX * ( mass - otherMass ) ) + ( 2 * otherMass * otherVelocityX ) ) / sum;
                double tmpY0 = ( ( velocityY * ( mass - otherMass ) ) + ( 2 * otherMass * otherVelocityY ) ) / sum;
                double tmpX1 = ( ( otherVelocityX * ( otherMass - mass ) ) + ( 2 * mass * velocityX ) ) / sum;
                double tmpY1 = ( ( otherVelocityY * ( otherMass - mass ) ) + ( 2 * mass * velocityY ) ) / sum;
                setVelocity( tmpX0, tmpY0 );
                other.setVelocity( tmpX1, tmpY1 );
            }
            else
            {
                state = COLLISION_IN;
            }
        }
        else
        {
            state = COLLISION_NO;
            collisions.remove( other );
            other.removeCollision( this );
        }
        return state;
    }

    // Returns true if this Element hits the edge of the given rectangle
    public boolean collide( int w, int h )
    {
        boolean hit = false;
        if ( ( centerX + radius ) >= w )
        {
            centerX = w - radius;
            setVelocity( -velocityX, velocityY );
            hit = true;
        }
        else if ( ( centerX - radius ) <= 0 )
        {
            centerX = radius;
            setVelocity( -velocityX, velocityY );
            hit = true;
        }
        if ( ( centerY + radius ) >= h )
        {
            centerY = h - radius;
            setVelocity( velocityX, -velocityY );
            hit = true;
        }
        else if ( ( centerY - radius ) <= 0 )
        {
            centerY = radius;
            setVelocity( velocityX, -velocityY );
            hit = true;
        }
        return hit;
    }

    public void draw( Canvas canvas, Paint paint, Bitmap background )
    {
        int r = ( int )radius;

// pulse the radius
if(flashes.size() > 0){
    switch(drawCounter){
    case 0:
        r -= 2;
        drawCounter++;
        break;
    case 1:
        r += 2;
        drawCounter = 0;
        break;
    }
}

        // draw depending on type
        switch ( type )
        {
        case TYPE_DIAGONAL:
        {
            Paint.Style style = paint.getStyle();
            int cp = paint.getColor();
//            paint.setShader( new RadialGradient( ( int )centerX, ( int )centerY, ( int )r, colors, pos, Shader.TileMode.CLAMP ) );
//            paint.setStyle( Paint.Style.FILL );
            paint.setStyle( Paint.Style.STROKE );
//            paint.setStrokeWidth( 2 );
            paint.setColor( colorDiagonal );
            canvas.drawCircle( ( int )centerX, ( int )centerY, ( int )r, paint );
            paint.setStyle( style );
            paint.setColor( cp );
//            paint.setShader( null );
            break;
        }
        case TYPE_PLUS:
        {
            Paint.Style style = paint.getStyle();
            int cp = paint.getColor();
//            paint.setStyle( Paint.Style.FILL );
            paint.setStyle( Paint.Style.STROKE );
            paint.setColor( colorPlus );
//            paint.setStrokeWidth( 2 );
            canvas.drawCircle( ( int )centerX, ( int )centerY, ( int )r, paint );
            paint.setColor( cp );
            paint.setStyle( style );
            break;
        }
        }
    }

    public void drawFlashes( Canvas c, Paint p, int fcolor )
    {
        if ( flashes.size() > 0 )
        {
            float sw = p.getStrokeWidth();
            Paint.Style s = p.getStyle();
            int cp = p.getColor();
            for ( FlashPoint fp : flashes )
            {
                if ( fp.draw( c, p, fcolor ) )
                {
                    flashesDone.add( fp );
                }
            }
            flashes.removeAll( flashesDone );
            flashesDone.clear();
            p.setStrokeWidth( sw );
            p.setColor( cp );
            p.setStyle( s );
        }
    }

    public boolean addCollision( Element element )
    {
        return collisions.add( element );
    }

    public boolean collidesWith( Element element )
    {
        return collisions.contains( element );
    }

    public boolean removeCollision( Element element )
    {
        return collisions.remove( element );
    }

    public void addFlash()
    {
        if ( type == TYPE_DIAGONAL )
        {
            flashes.add( new FlashPoint( this, collisionColorDiagonal, flashColorDiagonal ) );
        }
        else
        {
            flashes.add( new FlashPoint( this, collisionColorPlus, flashColorPlus ) );
        }
    }

    public double centerSqDistance( Element element )
    {
        double dx = centerX - element.getCenterX();
        double dy = centerY - element.getCenterY();
        return ( ( dx * dx ) + ( dy * dy ) );
    }

    public double getCenterY()
    {
        return centerY;
    }

    public double getCenterX()
    {
        return centerX;
    }

    public void update()
    {
        age++;
        centerX += velocityX;
        centerY += velocityY;
    }

    public final static void setColorDiagonal( final int color )
    {
        colorDiagonal = MASK_COLOR_ALPHA | color;
    }

    public final static void setColorPlus( final int color )
    {
        colorPlus = MASK_COLOR_ALPHA | color;
    }

    public final static void setFlashColorDiagonal( final int color )
    {
        flashColorDiagonal = MASK_COLOR_ALPHA | color;
    }

    public final static void setFlashColorPlus( final int color )
    {
        flashColorPlus = MASK_COLOR_ALPHA | color;
    }

    public final static void setCollisionColorDiagonal( final int color )
    {
        collisionColorDiagonal = MASK_COLOR_ALPHA | color;
    }

    public final static void setCollisionColorPlus( final int color )
    {
        collisionColorPlus = MASK_COLOR_ALPHA | color;
    }

}
