package com.derek.android.wallpapers.my_lwp;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import java.util.ArrayList;
import java.util.List;


public class LiveWallpaperImpl
{
    public static final String SHARED_PREFS_NAME = "circles_lwp_settings";

    private static final int RADIUS_MIN = 1;
    private static final int RADIUS_MAX = 200;
    private static final int RADIUS_RANGE = RADIUS_MAX - RADIUS_MIN;

    private static final double SPEED_DIAGONAL = 5;
    private static final double SPEED_PLUS = 5;

    private static final int AGE_MIN = 20; //?
    public static final int AGE_MAX = 2000;
    public static final int AGE_RANGE = AGE_MAX - AGE_MIN;
    public static final int NB_MIN = 8;
    public static final int NB_MAX = 200;
    private static final int NB_RANGE = NB_MAX - NB_MIN;

    private static final int DEFAULT_COLOR_FLASH = 0xFF0000;


    private int width;
    private int height;

    private float touchDownY;
    private float touchDownX;
    private float touchUpX;
    private float touchUpY;


    private int maxNbElements;
    private int maxAgeElements;
    private final List<Element> elements;
    private List<Element> collisionElements;
    private List<Element> toDeleteElements;

    private int radiusDiagonal;
    private int radiusPlus;
    private double speedDiagonal;
    private double speedPlus;

    private boolean showCircles;
    private boolean showTrails;

    private int flashColor;


    public LiveWallpaperImpl( Context context )
    {
        maxNbElements = ( NB_MIN + NB_MAX ) / 2;
        maxAgeElements = ( AGE_MIN + AGE_MAX ) / 2;
        elements = new ArrayList<Element>();
        collisionElements = new ArrayList<Element>();
        toDeleteElements = new ArrayList<Element>();
    }

    public void init( final int width, final int height )
    {
        this.width = width;
        this.height = height;

        touchDownY = 0.0f;
        touchDownX = 0.0f;
        touchUpX = 0.0f;
        touchUpY = 0.0f;

//?        Element.init();

        elements.clear();
        collisionElements.clear();
        toDeleteElements.clear();
    }


    public void preUpdate()
    {
        // check for collisions
        collisionElements.clear();

        List<Element> elements2 = new ArrayList<Element>( elements );
        for ( Element element : elements )
        {
            element.collide( width, height );

            elements2.remove( element );
            for ( Element element2 : elements2 )
            {
                int state = element2.collide( element );
                if ( state != Element.COLLISION_NO )
                {
                    if ( element.getType() != element2.getType() )
                    {
                        collisionElements.add( element2 );
                    }
                    else
                    {
//                        element.die(); //?
                        toDeleteElements.add( element );
// !!!! ???? TODO: OK ? ???? !!!!
// => ok if elements are removed twice?
//                        toDeleteElements.add( element2 );
                    }
                }
            }
        }
        elements.removeAll( toDeleteElements );
        toDeleteElements.clear();
    }

    public void postUpdate()
    {
        // delete elements that are too old
        int nb = elements.size();
        if ( nb > 0 )
        {
            int ndeleted = 0;
            for ( Element element : elements )
            {
                if ( element.getAge() > maxAgeElements )
                {
                    if ( !collisionElements.contains( element ) )
                    {
                        toDeleteElements.add( element );
                        ndeleted++;
                        if ( ndeleted >= nb )
                        {
                            break;
                        }
                    }
                }
            }
            elements.removeAll( toDeleteElements );
            toDeleteElements.clear();
        }

        // delete the elements that are too many
        nb = elements.size();
        if ( nb > maxNbElements )
        {
            int nToDelete = nb - maxNbElements;
            Element[] elements2 = elements.toArray( new Element[ 1 ] );
            int ndeleted = 0;
            for ( int i = 0; i < nb; i++ )
            {
                if ( !collisionElements.contains( elements2[ i ] ) )
                {
                    toDeleteElements.add( elements2[ i ] );
                    ndeleted++;
                    if ( ndeleted >= nToDelete )
                    {
                        break;
                    }
                }
            }
            elements.removeAll( toDeleteElements );
            toDeleteElements.clear();
        }

        for ( Element element : elements )
        {
            element.update();
        }
    }

    public void drawOnBackground( Canvas c, Paint paint, Bitmap bitmap )
    {
        for ( Element element : elements )
        {
            element.drawFlashes( c, paint, flashColor );
        }
        if ( showTrails && showCircles )
        {
            for ( Element element : elements )
            {
                element.draw( c, paint, bitmap );
            }
        }
    }

    public void draw( Canvas c, Paint paint, Bitmap bitmap )
    {
        if ( showCircles && !showTrails )
        {
            for ( Element element : elements )
            {
                element.draw( c, paint, bitmap );
            }
        }
    }

    public void touchEventDown( final float x, final float y )
    {
        touchDownX = x;
        touchDownY = y;

        generateElement( Element.TYPE_DIAGONAL, touchDownX, touchDownY );
    }

    public void touchEventUp( final float x, final float y )
    {
        touchUpX = x;
        touchUpY = y;

        generateElement( Element.TYPE_PLUS, touchUpX, touchUpY );
    }


    private void generateElement( final int type, final float x, final float y )
    {
        if ( type == Element.TYPE_DIAGONAL )
        {
            Element element = new Element( type, ( int )( x + radiusDiagonal + 1 ), ( int )( y + radiusDiagonal + 1 ), radiusDiagonal );
            element.setVelocity( speedDiagonal, speedDiagonal );
            elements.add( element );

            element = new Element( type, ( int )( x + radiusDiagonal + 1 ), ( int )( y - radiusDiagonal - 1 ), radiusDiagonal );
            element.setVelocity( speedDiagonal, -speedDiagonal );
            elements.add( element );

            element = new Element( type, ( int )( x - radiusDiagonal - 1 ), ( int )( y - radiusDiagonal - 1 ), radiusDiagonal );
            element.setVelocity( -speedDiagonal, -speedDiagonal );
            elements.add( element );

            element = new Element( type, ( int )( x - radiusDiagonal - 1 ), ( int )( y + radiusDiagonal + 1 ), radiusDiagonal );
            element.setVelocity( -speedDiagonal, speedDiagonal );
            elements.add( element );
        }
        else if ( type == Element.TYPE_PLUS )
        {
            Element element = new Element( type, ( int )( x ), ( int )( y + radiusPlus + 5 ), radiusPlus );
            element.setVelocity( 0.0f, speedPlus );
            elements.add( element );

            element = new Element( type, ( int )( x + radiusPlus + 5 ), ( int )( y ), radiusPlus );
            element.setVelocity( speedPlus, 0.0f );
            elements.add( element );

            element = new Element( type, ( int )( x ), ( int )( y - radiusPlus - 5 ), radiusPlus );
            element.setVelocity( 0.0f, -speedPlus );
            elements.add( element );

            element = new Element( type, ( int )( x - radiusPlus - 5 ), ( int )( y ), radiusPlus );
            element.setVelocity( -speedPlus, 0.0f );
            elements.add( element );
        }
    }

    public void setPreference( SharedPreferences prefs, String key )
    {
        if ( key.equals( SettingsActivity.PREF_GENERAL_MAX_NB ) )
        {
            changeMaxNbPref( prefs.getInt( SettingsActivity.PREF_GENERAL_MAX_NB, 50 ) );
        }
        else if ( key.equals( SettingsActivity.PREF_GENERAL_MAX_AGE ) )
        {
            changeMaxAgePref( prefs.getInt( SettingsActivity.PREF_GENERAL_MAX_AGE, 50 ) );
        }
        else if ( key.equals( SettingsActivity.PREF_OBJECTS_SHOW_CIRCLES ) )
        {
            changeShowCirclesPref( prefs.getBoolean( SettingsActivity.PREF_OBJECTS_SHOW_CIRCLES, true ) );
        }
        else if ( key.equals( SettingsActivity.PREF_OBJECTS_SHOW_TRAILS ) )
        {
            changeShowTrailsPref( prefs.getBoolean( SettingsActivity.PREF_OBJECTS_SHOW_TRAILS, false ) );
        }
        else if ( key.equals( SettingsActivity.PREF_OBJECTS_COLOR_DIAGONAL ) )
        {
            changeColorDiagonalPref( prefs.getInt( SettingsActivity.PREF_OBJECTS_COLOR_DIAGONAL, Element.DEFAULT_COLOR_DIAGONAL ) );
        }
        else if ( key.equals( SettingsActivity.PREF_OBJECTS_RADIUS_DIAGONAL ) )
        {
            changeRadiusDiagonalPref( prefs.getInt( SettingsActivity.PREF_OBJECTS_RADIUS_DIAGONAL, Element.DEFAULT_RADIUS ) );
        }
        else if ( key.equals( SettingsActivity.PREF_OBJECTS_COLOR_PLUS ) )
        {
            changeColorPlusPref( prefs.getInt( SettingsActivity.PREF_OBJECTS_COLOR_PLUS, Element.DEFAULT_COLOR_PLUS ) );
        }
        else if ( key.equals( SettingsActivity.PREF_OBJECTS_RADIUS_PLUS ) )
        {
            changeRadiusPlusPref( prefs.getInt( SettingsActivity.PREF_OBJECTS_RADIUS_PLUS, Element.DEFAULT_RADIUS ) );
        }
// !!!! TODO: add preferences for collisions & flashes colors !!!!
//...
        else if ( key.equals( SettingsActivity.PREF_OBJECTS_COLOR_FLASH ) )
        {
            changeColorFlashPref( prefs.getInt( SettingsActivity.PREF_OBJECTS_COLOR_FLASH, DEFAULT_COLOR_FLASH ) );
        }
    }

    public void loadPreferences( SharedPreferences prefs )
    {
        changeMaxNbPref( prefs.getInt( SettingsActivity.PREF_GENERAL_MAX_NB, 50 ) );
        changeMaxNbPref( prefs.getInt( SettingsActivity.PREF_GENERAL_MAX_AGE, 50 ) );
        changeShowCirclesPref( prefs.getBoolean( SettingsActivity.PREF_OBJECTS_SHOW_CIRCLES, true ) );
        changeShowTrailsPref( prefs.getBoolean( SettingsActivity.PREF_OBJECTS_SHOW_TRAILS, false ) );
        changeRadiusDiagonalPref( prefs.getInt( SettingsActivity.PREF_OBJECTS_RADIUS_DIAGONAL, Element.DEFAULT_RADIUS ) );
        changeColorDiagonalPref( prefs.getInt( SettingsActivity.PREF_OBJECTS_COLOR_DIAGONAL, Element.DEFAULT_COLOR_DIAGONAL ) );
        changeRadiusPlusPref( prefs.getInt( SettingsActivity.PREF_OBJECTS_RADIUS_PLUS, Element.DEFAULT_RADIUS ) );
        changeColorPlusPref( prefs.getInt( SettingsActivity.PREF_OBJECTS_COLOR_PLUS, Element.DEFAULT_COLOR_PLUS ) );
        changeColorFlashPref( prefs.getInt( SettingsActivity.PREF_OBJECTS_COLOR_FLASH, DEFAULT_COLOR_FLASH ) );

// !!!! ???? TODO: add as preferences ? ???? !!!!
        speedDiagonal = SPEED_DIAGONAL;
        speedPlus = SPEED_PLUS;
    }

    private void changeMaxNbPref( int value )
    {
        maxNbElements = NB_MIN + ( int )( ( value / 100.0f ) * NB_RANGE );
    }

    private void changeMaxAgePref( int value )
    {
        maxAgeElements = AGE_MIN + ( int )( ( value / 100.0f ) * AGE_RANGE );
    }

    private void changeShowCirclesPref( boolean value )
    {
        showCircles = value;
        if ( !showCircles )
        {
            showTrails = false;
        }
    }

    private void changeShowTrailsPref( boolean value )
    {
        showTrails = value;
    }

    private void changeRadiusDiagonalPref( int value )
    {
        radiusDiagonal = RADIUS_MIN + ( int )( ( value / 100.0f ) * RADIUS_RANGE );
    }

    private void changeColorDiagonalPref( int value )
    {
        Element.setColorDiagonal( value );
    }

    private void changeRadiusPlusPref( int value )
    {
        radiusPlus = RADIUS_MIN + ( int )( ( value / 100.0f ) * RADIUS_RANGE );
    }

    private void changeColorPlusPref( int value )
    {
        Element.setColorPlus( value );
    }

    private void changeColorFlashPref( int value )
    {
        flashColor = value;
    }

}
