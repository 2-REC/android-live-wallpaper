package com.derek.android.wallpapers.my_lwp;

import android.content.Context;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;


public class SettingsFragmentGeneral extends PreferenceFragment
                                     implements PreferenceYesNoDialog.YesNoDialogListener
{
    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        getPreferenceManager().setSharedPreferencesMode( Context.MODE_PRIVATE );
        getPreferenceManager().setSharedPreferencesName( LiveWallpaperImpl.SHARED_PREFS_NAME );

        addPreferencesFromResource( R.xml.prefs_general );

        Preference resetSettings = getPreferenceManager().findPreference( SettingsActivity.PREF_GENERAL_RESET );
        if ( resetSettings != null )
        {
            PreferenceYesNoDialog resetDlg = ( PreferenceYesNoDialog )resetSettings;
            resetDlg.setListener( this );
        }
    }

    public void onDialogClosed( boolean positiveResult )
    {
        if ( positiveResult )
        {
            SettingsActivity activity = ( SettingsActivity )getActivity();
            activity.resetSettings();
        }
    }

}
