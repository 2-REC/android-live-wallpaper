package com.derek.android.wallpapers.my_lwp;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
//////// SENSOR - MID
/*
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
*/
//////// SENSOR - END
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.view.MotionEvent;
import android.view.SurfaceHolder;


/**
 * Live Wallpaper service.
 */
public class MyLwpService extends WallpaperService
{
// !!!! ???? TODO: add settings for frame interval ? ???? !!!!
    private static final int FRAME_INTERVAL = 20;
    private static final int ALPHA_MAX = 255;
    private static final int ALPHA = 70;
    private static final int COLOR_BACKGROUND = 0x000000;


    @Override
    public Engine onCreateEngine()
    {
        return new MyLwpEngine( this );
    }

    public class MyLwpEngine extends Engine
//////// SENSOR - BEGIN
                              implements OnSharedPreferenceChangeListener
//////// SENSOR - MID
//                              implements OnSharedPreferenceChangeListener, SensorEventListener
//////// SENSOR - END
    {
        private final Handler handler = new Handler();

        private final Runnable updateRunner = new Runnable()
            {
                @Override
                public void run()
                {
                    update();
                }
            };

        private WallpaperService context;
        private SharedPreferences prefs;

//////// SENSOR - MID
//        private SensorManager sensorManager;
//////// SENSOR - END

        private Canvas canvas;
        private final Paint paint;
        private Bitmap bitmap;
        private int alpha;

        private int backgroundColor;
        private String backgroundImagePath;
        private Bitmap backgroundImageBitmap;
        private boolean backgroundImageUse;

        private boolean visible = true;
        private int width;
        private int height;
        private float offsetX;
        private float offsetY;
        private int offsetPixelX;
        private int offsetPixelY;

//////// SLIDE - BEGIN
        private float lastTouchX;
        //private float lastTouchY;
        private int maxOffsetPixelX;
        private int activePointerId = -1;
//////// SLIDE - END

        private LiveWallpaperImpl impl;


        public MyLwpEngine( WallpaperService ws )
        {
            context = ws;

            canvas = null;
            paint = new Paint();
            bitmap = null;
// !!!! TODO: adapt ... !!!!
            paint.setColor( 0xffffffff );
            paint.setAntiAlias( true );
            paint.setStrokeWidth( 2 );
            paint.setStrokeCap( Paint.Cap.ROUND );
            paint.setStyle( Paint.Style.STROKE );
            alpha = ALPHA;

            backgroundColor = COLOR_BACKGROUND;
            backgroundImagePath = null;
            backgroundImageBitmap = null;
            backgroundImageUse = false;

            offsetX = 0.0f;
            offsetY = 0.0f;
            maxOffsetPixelX = 0;
            offsetPixelX = 0;
            offsetPixelY = 0;

            impl = new LiveWallpaperImpl( MyLwpService.this );

            prefs = MyLwpService.this.getSharedPreferences( LiveWallpaperImpl.SHARED_PREFS_NAME, 0 );
            prefs.registerOnSharedPreferenceChangeListener( this );
            onSharedPreferenceChanged( prefs, null );
        }

        @Override
        public void onCreate( SurfaceHolder surfaceHolder )
        {
            super.onCreate( surfaceHolder );
            setTouchEventsEnabled( true );
//////// SENSOR - MID
//            sensorManager = ( SensorManager )getSystemService( SENSOR_SERVICE );
//////// SENSOR - END
        }

        @Override
        public void onDestroy()
        {
            super.onDestroy();
            visible = false;
            handler.removeCallbacks( updateRunner );

//////// SENSOR - MID
/*
// !!!! ???? TODO: OK here ? ???? !!!!
            if ( sensorManager != null )
            {
                sensorManager.unregisterListener( this );
            }
*/
//////// SENSOR - END
        }

        @Override
        public void onVisibilityChanged( boolean visible )
        {
            this.visible = visible;
            if ( visible )
            {
//////// SENSOR - MID
/*
// !!!! ???? TODO: OK here ? ???? !!!!
                if ( sensorManager != null )
                {
// !!!! ???? TODO: what sensor type to register ? ???? !!!!
                    Sensor orientation = sensorManager.getDefaultSensor( Sensor.TYPE_ORIENTATION );
                    if ( orientation != null )
                    {
                        sensorManager.registerListener( this,
                                                        orientation,
                                                        SensorManager.SENSOR_DELAY_GAME,
                                                        null );
                    }
                }
*/
//////// SENSOR - END
                update();
            }
            else
            {
                handler.removeCallbacks( updateRunner );
//////// SENSOR - MID
/*
// !!!! ???? TODO: OK here ? ???? !!!!
                if ( sensorManager != null )
                {
                    sensorManager.unregisterListener( this );
                }
*/
//////// SENSOR - END
            }
        }

        @Override
        public void onSurfaceChanged( SurfaceHolder holder, int format, int width, int height )
        {
            super.onSurfaceChanged( holder, format, width, height );

            this.width = width;
            this.height = height;

//////// SENSOR - MID
// !!!! ???? TODO: OK here ? ???? !!!!
//            if ( sensorManager != null )
//            {
// !!!! ???? TODO: what sensor type to register ? ???? !!!!
//                Sensor orientation = sensorManager.getDefaultSensor( Sensor.TYPE_ORIENTATION );
//                if ( orientation != null )
//                {
//                    sensorManager.registerListener( this,
//                                                    orientation,
//                                                    SensorManager.SENSOR_DELAY_GAME,
//                                                    null );
//                }
//            }
//////// SENSOR - END

            canvas = createCanvasBuffer( format, width, height );

            impl.init( width, height );

    		loadBgBitmap();
        }

/*
        @Override
        public void onSurfaceCreated( SurfaceHolder holder )
        {
            super.onSurfaceCreated( holder );
            return;
        }
*/

        @Override
        public void onSurfaceDestroyed( SurfaceHolder holder )
        {
            super.onSurfaceDestroyed( holder );
            visible = false;
            handler.removeCallbacks( updateRunner );
            prefs.unregisterOnSharedPreferenceChangeListener( this );

//////// SENSOR - MID
/*
// !!!! ???? TODO: OK here ? ???? !!!!
            if ( sensorManager != null )
            {
                sensorManager.unregisterListener( this );
            }
*/
//////// SENSOR - END
        }

        @Override
        public void onOffsetsChanged( float xOffset, float yOffset, float xStep, float yStep, int xPixels, int yPixels )
        {
            offsetX = xOffset;
            offsetY = yOffset;

            if ( backgroundImageBitmap != null )
            {
                offsetPixelX = ( int )( offsetX * ( width - backgroundImageBitmap.getWidth() ) );
                offsetPixelY = ( int )( offsetY * ( height - backgroundImageBitmap.getHeight() ) );
                drawFullBackground();
            }
            update();
            return;
        }


// !!!! ???? TODO: needed ? ???? !!!!
/*
        public Bundle onCommand( String action, int x, int y, int z, Bundle extras, boolean resultRequested )
        {
// !!!! TODO: do something !!!!
            return super.onCommand( action, x, y, z, extras, resultRequested );
        }
*/


    	@Override
    	public void onTouchEvent( MotionEvent event )
        {
            switch ( event.getAction() )
            {
            case MotionEvent.ACTION_DOWN:
            {
//////// SLIDE - BEGIN
                lastTouchX = event.getX();
                //lastTouchY = event.getY();
                activePointerId = event.getPointerId( event.getActionIndex() );
//////// SLIDE - END

                impl.touchEventDown( event.getX(), event.getY() );
                break;
            }
            case MotionEvent.ACTION_UP:
            {
//////// SLIDE - BEGIN
//                activePointerId = MotionEvent.INVALID_POINTER_ID;
                activePointerId = -1;
//////// SLIDE - END

                impl.touchEventUp( event.getX(), event.getY() );
                break;
            }
//////// SLIDE - BEGIN
            case MotionEvent.ACTION_MOVE:
            {
                final int pointerIndex = event.findPointerIndex( activePointerId );
                if ( pointerIndex == event.getActionIndex() )
                {
                    final float touchX = event.getX();
                    //final float touchY = event.getY();
                    final float dx = touchX - lastTouchX;
                    //final float dy = touchY - lastTouchY;

                    offsetPixelX += ( int )dx;
                    if ( offsetPixelX > 0 )
                    {
                    	offsetPixelX = 0;
                    }
                    else if ( offsetPixelX < maxOffsetPixelX )
                    {
                    	offsetPixelX = maxOffsetPixelX;
                    }
                    // no vertical movement
                    //offsetPixelY += ( int )dy;

                    lastTouchX = touchX;
                    //lastTouchY = touchY;

                    if ( backgroundImageBitmap != null )
                    {
                        drawFullBackground();
                    }
                    update();
                }
                break;
            }
            case MotionEvent.ACTION_CANCEL:
            {
//                activePointerId = MotionEvent.INVALID_POINTER_ID;
                activePointerId = -1;
                break;
            }
//////// SLIDE - END
            }

            super.onTouchEvent( event );
        }


//////// SENSOR - MID
/*
        @Override
        public void onAccuracyChanged( Sensor arg0, int arg1 )
        {
        }

// !!!! TODO: add orientation events ... !!!!
        @Override
        public void onSensorChanged( SensorEvent event )
        {
//            synchronized( this )
//            {
//                switch ( event.sensor.getType() )
//                {
//                case Sensor.TYPE_ORIENTATION:
//                    //pitch = event.values[ 1 ];
//                    break;
//                }
//            }

// !!!! ???? TODO: needed ? ???? !!!!
synchronized( this )
{
            int type = event.sensor.getType();
            if (type == Sensor.TYPE_ACCELEROMETER)
            {
                float x = event.values[ 0 ];
                float y = event.values[ 1 ];
                float z = event.values[ 2 ];
// set velocity
for ( Minion minion : minions )
{
    minion.setVelocity( -x, y );
}
            }



//            int type = event.sensor.getType();
//
//            // smoothing sensor data
//            if ( type == Sensor.TYPE_MAGNETIC_FIELD )
//            {
//geomag[0]=(geomag[0]*1+evt.values[0])*0.5f;
//geomag[1]=(geomag[1]*1+evt.values[1])*0.5f;
//geomag[2]=(geomag[2]*1+evt.values[2])*0.5f;
//            }
//            else if (type == Sensor.TYPE_ACCELEROMETER)
//            {
//gravity[0]=(gravity[0]*2+evt.values[0])*0.33334f;
//gravity[1]=(gravity[1]*2+evt.values[1])*0.33334f;
//gravity[2]=(gravity[2]*2+evt.values[2])*0.33334f;
//            }
//
//            if ( ( type == Sensor.TYPE_MAGNETIC_FIELD ) || ( type == Sensor.TYPE_ACCELEROMETER ) )
//            {
//rotationMatrix = new float[16];
//                SensorManager.getRotationMatrix( rotationMatrix, null, gravity, geomag );
//SensorManager.remapCoordinateSystem( 
//rotationMatrix, 
//SensorManager.AXIS_Y, 
//SensorManager.AXIS_MINUS_X, 
//rotationMatrix );
//            }


}
        }
*/
//////// SENSOR - END


        private Canvas createCanvasBuffer( int fmt, int w, int h )
        {
            bitmap = Bitmap.createBitmap( w, h, Bitmap.Config.ARGB_8888 );
            Canvas newCanvas = new Canvas( bitmap );
            return newCanvas;
        }

        private void loadBgBitmap()
        {
            Bitmap bmp = null;
            if ( ( backgroundImagePath != null ) && ( width > 0 ) && ( height > 0 ) )
            {
                bmp = Utils.imageFilePathToBitmap( context, backgroundImagePath, Math.max( width, height ) );
                if ( bmp != null )
                {
//                    backgroundImageBitmap = scaleBgBitmap( bmp );
//                    backgroundImageBitmap = scaleBgBitmap( bmp, width, height );
//                    backgroundImageBitmap = Utils.scaleBgBitmap( bmp, width, height );
// !!!! TODO: handle orientation & make good ratio handling ... !!!!
                    final float ratio = height / bmp.getHeight();
                    backgroundImageBitmap = Utils.scaleBgBitmap( bmp, (int)( bmp.getWidth() * ratio ), height );
                    maxOffsetPixelX = width - backgroundImageBitmap.getWidth();
                }
                if ( backgroundImageBitmap != null )
                {
                    offsetPixelX = ( int )( offsetX * ( width - backgroundImageBitmap.getWidth() ) );
                    offsetPixelY = ( int )( offsetY * ( height - backgroundImageBitmap.getHeight() ) );
                }
            }
        }
/*
        private Bitmap loadBitmap( final int resource, final int w, final int h )
        {
            Bitmap bmp = null;
            if ( ( resource > 0 ) && ( w > 0 ) && ( h > 0 ) )
            {
                InputStream is = context.getResources().openRawResource( resource );
                try
                {
                    bmp = BitmapFactory.decodeStream( is );
                    if ( bmp != null )
                    {
//                        bmp = scaleBgBitmap( bmp );
                        bmp = scaleBgBitmap( bmp, w, h );
                    }
                }
                finally
                {
                    try
                    {
                        is.close();
                    }
                    catch( Exception e )
                    {
                    }
                }
            }
            return bmp;
        }
*/
/*
        private Bitmap scaleBgBitmap( Bitmap b )
        {
            int bw = b.getWidth();
            int bh = b.getHeight();
            double s = ( double )height / ( double )bh;
            int newW = ( int )( bw * s );
            if ( newW < width )
            {
                newW = width;
            }
            return Bitmap.createScaledBitmap( b, newW, height, false );
        }
*/
/*
        private Bitmap scaleBgBitmap( Bitmap b, int w, int h )
        {
            int bw = b.getWidth();
            int bh = b.getHeight();
            double s = ( double )h / ( double )bh;
            int newW = ( int )( bw * s );
            if ( newW < w )
            {
                newW = w;
            }
            return Bitmap.createScaledBitmap( b, newW, h, false );
        }
*/

        private void update()
        {
            final SurfaceHolder holder = getSurfaceHolder();

            impl.preUpdate();
            drawBackground();
            impl.postUpdate();
            impl.drawOnBackground( canvas, paint, bitmap );

            Canvas c = null;
            try
            {
                c = holder.lockCanvas();
                if ( c != null )
                {
                    draw( c );
                }
            }
            finally
            {
                if ( c != null )
                {
                    holder.unlockCanvasAndPost( c );
                }
            }

            handler.removeCallbacks( updateRunner );
            if ( visible )
            {
                handler.postDelayed( updateRunner, FRAME_INTERVAL );
            }
        }

        private void drawFullBackground()
        {
            if ( canvas != null )
            {
                paint.setAlpha( 0xff );
                canvas.drawBitmap( backgroundImageBitmap, offsetPixelX, offsetPixelY, paint );
            }
        }

        private void drawBackground()
        {
            if ( backgroundImageUse && ( backgroundImagePath != null ) )
            {
                if ( backgroundImageBitmap == null )
                {
                    loadBgBitmap();
                }
            }
            // fade-erase
            if ( backgroundImageUse && ( backgroundImageBitmap != null ) )
            {
                paint.setAlpha( alpha );
                canvas.drawBitmap( backgroundImageBitmap, offsetPixelX, offsetPixelY, paint );
                paint.setAlpha( 0xff );
            }
            else
            {
                canvas.drawARGB( alpha, 0, 0, backgroundColor );
                paint.setAlpha( 0xff );
            }
        }

        @Override
        public void onSharedPreferenceChanged( SharedPreferences prefs, String key )
        {
            if ( key != null )
            {
                if ( key.equals( SettingsActivity.PREF_BG_IMAGE ) )
                {
                    changeBgImagePref( prefs.getString( SettingsActivity.PREF_BG_IMAGE, null ) );
                }
                else if ( key.equals( SettingsActivity.PREF_BG_USE_IMAGE ) )
                {
                    changeUseImagePref( prefs.getBoolean( SettingsActivity.PREF_BG_USE_IMAGE, false ) );
                }
                else if ( key.equals( SettingsActivity.PREF_BG_COLOR ) )
                {
                    changeBackgroundPref( prefs.getInt( SettingsActivity.PREF_BG_COLOR, COLOR_BACKGROUND ) );
                }
                else if ( key.equals( SettingsActivity.PREF_GENERAL_ALPHA ) )
                {
                    changeAlphaPref( prefs.getInt( SettingsActivity.PREF_GENERAL_ALPHA, ALPHA ) );
                }
                else
                {
                    impl.setPreference( prefs, key );
                }
            }
            else
            {
                changeBgImagePref( prefs.getString( SettingsActivity.PREF_BG_IMAGE, null ) );
                changeUseImagePref( prefs.getBoolean( SettingsActivity.PREF_BG_USE_IMAGE, false ) );
                changeBackgroundPref( prefs.getInt( SettingsActivity.PREF_BG_COLOR, COLOR_BACKGROUND ) );
                changeAlphaPref( prefs.getInt( SettingsActivity.PREF_GENERAL_ALPHA, ALPHA ) );
                impl.loadPreferences( prefs );
            }
            return;
        }

        private void changeBgImagePref( String value )
        {
            backgroundImagePath = value;
            if ( backgroundImageUse )
            {
                loadBgBitmap();
            }
        }

        private void changeUseImagePref( boolean value )
        {
        	backgroundImageUse = value;
            if ( !backgroundImageUse )
            {
            	backgroundImageBitmap = null;
            }
        }

        private void changeBackgroundPref( int value )
        {
            backgroundColor = value;
        }

        private void changeAlphaPref( int value )
        {
            alpha = ( int )( ( ( 100 - value ) / 100.0f ) * ALPHA_MAX );
        }

        protected void draw( Canvas c )
        {
            c.drawBitmap( bitmap, 0, 0, paint );
            impl.draw( c, paint, bitmap );
        }

    }

}
