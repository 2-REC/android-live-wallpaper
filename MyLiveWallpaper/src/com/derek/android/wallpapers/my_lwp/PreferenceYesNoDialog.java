package com.derek.android.wallpapers.my_lwp;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;


public class PreferenceYesNoDialog extends DialogPreference
{
    private YesNoDialogListener mListener;

    public abstract interface YesNoDialogListener
    {
        public abstract void onDialogClosed( boolean positiveResult );
    }

    public PreferenceYesNoDialog( Context context, AttributeSet attrs )
    {
        this( context, attrs, android.R.attr.yesNoPreferenceStyle );
    }

    public PreferenceYesNoDialog( Context context, AttributeSet attrs, int defStyle )
    {
        super( context, attrs, defStyle );
    }

    public PreferenceYesNoDialog( Context context )
    {
        this( context, null );
    }

    public void setListener( YesNoDialogListener listener )
    {
        mListener = listener;
    }

    @Override
    protected void onDialogClosed( boolean positiveResult )
    {
        if ( mListener != null )
        {
            mListener.onDialogClosed( positiveResult );
        }
    }

}
