package com.derek.android.wallpapers.my_lwp;

import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.View;
import android.preference.DialogPreference;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.LinearLayout;
import android.graphics.PorterDuff;

public class PreferenceColor extends DialogPreference
                             implements SeekBar.OnSeekBarChangeListener
{
    private static final String androidns = "http://schemas.android.com/apk/res/android";

    private Context context;
    private SeekBar seekBarR;
    private SeekBar seekBarG;
    private SeekBar seekBarB;
    private int defaultValueAttrs;
    private int max = 255;
    private int valueColor = 0;
    private int valueRed = 0; 
    private int valueGreen = 0;
    private int valueBlue = 0;
    private ImageView colorView;


    public PreferenceColor( Context context, AttributeSet attrs )
    { 
        super( context, attrs );
        this.context = context;
        defaultValueAttrs = attrs.getAttributeIntValue( androidns, "defaultValue", 0 );
        max = attrs.getAttributeIntValue( androidns, "max", 255 );
    }

    @Override
    public void onClick( DialogInterface dialog, int which )
    {
        if ( which == DialogInterface.BUTTON_POSITIVE )
        {
            if ( shouldPersist() )
            {
                persistInt( valueColor );
            }
        }
    }

    @Override 
    protected View onCreateDialogView()
    {
// !!!! TODO: adapt values !!!!
        LinearLayout.LayoutParams params;
        LinearLayout layout = new LinearLayout( context );
        layout.setOrientation( LinearLayout.VERTICAL );
        layout.setPadding( 6, 6, 6, 6 );

        colorView = new ImageView( context );
        colorView.setMinimumHeight( 100 );
        colorView.setBackgroundColor( 0xffff0000 );

        params = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT );
        params.bottomMargin = 25;

        layout.addView( colorView, params );

        seekBarR = new SeekBar( context );
        seekBarR.setOnSeekBarChangeListener( this );
        seekBarR.getProgressDrawable().setColorFilter( 0xbbff0000, PorterDuff.Mode.SRC_OVER );
        layout.addView( seekBarR, new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT ) );

        seekBarG = new SeekBar( context );
        seekBarG.setOnSeekBarChangeListener( this );
        seekBarG.getProgressDrawable().setColorFilter( 0xbb00ff00, PorterDuff.Mode.SRC_OVER );
        layout.addView( seekBarG, new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT ) );

        seekBarB = new SeekBar( context );
        seekBarB.setOnSeekBarChangeListener( this );
        seekBarB.getProgressDrawable().setColorFilter( 0xbb0000ff, PorterDuff.Mode.SRC_OVER );
        layout.addView( seekBarB, new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT ) );

        if ( shouldPersist() )
        {
            valueColor = getPersistedInt( defaultValueAttrs );
            valueRed = ( valueColor & 0x00ff0000 ) >> 16;
            valueGreen = ( valueColor & 0x0000ff00 ) >> 8;
            valueBlue = valueColor & 0x000000ff;
        }

        seekBarR.setMax( max );
        seekBarR.setProgress( valueRed );
        seekBarG.setMax( max );
        seekBarG.setProgress( valueGreen );
        seekBarB.setMax( max );
        seekBarB.setProgress( valueBlue );
        colorView.setBackgroundColor( valueColor | 0xff000000 );

        return layout;
    }

    @Override 
    protected void onBindDialogView( View v )
    {
        super.onBindDialogView( v );

        seekBarR.setMax( max );
        seekBarR.setProgress( valueRed );
        seekBarG.setMax( max );
        seekBarG.setProgress( valueGreen );
        seekBarB.setMax( max );
        seekBarB.setProgress( valueBlue );
    }

    @Override
    protected void onSetInitialValue( boolean restore, Object defaultValue )
    {
        super.onSetInitialValue( restore, defaultValue );

        if ( restore )
        {
            valueColor = shouldPersist() ? getPersistedInt( defaultValueAttrs ) : 0;
            valueRed = ( valueColor & 0x00ff0000 ) >> 16;
            valueGreen = ( valueColor & 0x0000ff00 ) >> 8;
            valueBlue = valueColor & 0x000000ff;
        }
        else
        {
            valueColor = ( Integer )defaultValue;
            valueRed = ( valueColor & 0x00ff0000 ) >> 16;
            valueGreen = ( valueColor & 0x0000ff00 ) >> 8;
            valueBlue = valueColor & 0x000000ff;
        }
    }

    public void onProgressChanged( SeekBar seek, int value, boolean fromTouch )
    {
        int v = value;
        if ( seek == seekBarR )
        {
            v = ( valueColor & 0x00ffff ) | ( value << 16 );
        }
        else if( seek == seekBarG )
        {
            v = ( valueColor & 0xff00ff ) | ( value << 8 );
        }
        else if ( seek == seekBarB )
        {
            v = ( valueColor & 0xffff00 ) | ( value );
        }
        valueColor = v;
        colorView.setBackgroundColor( valueColor | 0xff000000 );
        callChangeListener( Integer.valueOf( value ) );
    }

    public void onStartTrackingTouch( SeekBar seek )
    {
    }

    public void onStopTrackingTouch( SeekBar seek )
    {
    }

    public void setMax( int max )
    {
        this.max = max; 
    }

    public int getMax()
    {
        return max;
    }

}
