package com.derek.android.wallpapers.my_lwp;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;

public class FlashPoint
{
    private static final int  NB_FRAMES = 5;

    private static final Paint paint = new Paint();
//    private static final BlurMaskFilter blur = new BlurMaskFilter( BLUR_RADIUS, BlurMaskFilter.Blur.NORMAL );

    private RadialGradient shader;
    private int x;
    private int y;
    private int radius;
    private int count;

    static
    {
//        paint.setMaskFilter( blur );
        paint.setStyle( Paint.Style.STROKE );
    }


    public FlashPoint( int x, int y, int radius, int c1, int c2 )
    {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.count = 0;

        shader = new RadialGradient( x, y,
                                     radius + NB_FRAMES,
                                     Color.argb( 255, Color.red( c1 ), Color.green( c1 ), Color.blue( c1 ) ),
                                     Color.argb( 255, Color.red( c2 ), Color.green( c2 ), Color.blue( c2 ) ),
                                     Shader.TileMode.CLAMP );
    }

    public FlashPoint( Element element, int c1, int c2 )
    {
        this( ( int )element.getCenterX(), ( int )element.getCenterY(), ( int )element.getRadius(), c1, c2 );
    }

    // Returns true when done flashing.
    // The caller should stop calling and delete the object.
    public boolean draw( Canvas c, Paint p, int fcolor )
    {
        count++;
        if ( count == 1 )
        {
            p.setStyle( Paint.Style.FILL );
            p.setShader( shader );
            c.drawCircle( x, y, radius + NB_FRAMES, p );
            p.setStyle( Paint.Style.STROKE );
            p.setShader( null );
        }
        else
        {
            paint.setColor( Color.argb( 255 / ( count >> 1 ), Color.red( fcolor ), Color.green( fcolor ), Color.blue( fcolor ) ) );
            paint.setStrokeWidth( count * 4 );
            c.drawCircle( x, y, radius * count, paint );
        }
        return ( count >= FlashPoint.NB_FRAMES );
    }

}
