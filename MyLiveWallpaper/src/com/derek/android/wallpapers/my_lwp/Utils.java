package com.derek.android.wallpapers.my_lwp;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;


public class Utils
{
// !!!! TODO: might need to change that using interfaces in future versions of Android !!!!
    // given a content or file uri, return a file path
	@TargetApi( Build.VERSION_CODES.KITKAT )
    public static String uriToFilePath( final Context context, final Uri uri )
    {
        final boolean isKitKat = ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT );

        // DocumentProvider
        if ( isKitKat && DocumentsContract.isDocumentUri( context, uri ) )
        {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/"
                            + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] { split[1] };

                return getDataColumn(context, contentUri, selection,
                        selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri,
            String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = { column };

        try {
            cursor = context.getContentResolver().query(uri, projection,
                    selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument( Uri uri )
    {
        return "com.android.externalstorage.documents".equals( uri.getAuthority() );
    }

    public static boolean isDownloadsDocument( Uri uri )
    {
        return "com.android.providers.downloads.documents".equals( uri.getAuthority() );
    }

    public static boolean isMediaDocument( Uri uri )
    {
        return "com.android.providers.media.documents".equals( uri.getAuthority() );
    }

    public static boolean isGooglePhotosUri( Uri uri )
    {
        return "com.google.android.apps.photos.content".equals( uri.getAuthority() );
    }


    // given a file path, returns a bitmap
    public static Bitmap imageFilePathToBitmap( Context c, String path, int maxDim )
    {
        Bitmap bmp = null;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        try
        {
            // compute the smallest size bitmap we need to read
            BitmapFactory.decodeStream( new FileInputStream( path ), null, opts );
            int w = opts.outWidth;
            int h = opts.outHeight;
            int s = 1;
            while ( true )
            {
                if ( ( ( w / 2 ) < maxDim ) || ( ( h / 2 ) < maxDim ) )
                {
                    break;
                }
                w /= 2;
                h /= 2;
                s++;
            }
            // scale and read the data
            opts.inJustDecodeBounds = false;
            opts.inSampleSize = s;
            bmp = BitmapFactory.decodeStream( new FileInputStream( path ), null, opts );
        }
        catch ( FileNotFoundException e )
        {
            Log.w( "Utils", e.getMessage() );
        }
        return bmp;
    }

    public static Bitmap loadBitmap( final Context context, final int resource, final int w, final int h )
    {
        Bitmap bmp = null;
        if ( ( resource > 0 ) && ( w > 0 ) && ( h > 0 ) )
        {
            InputStream is = context.getResources().openRawResource( resource );
            try
            {
                bmp = BitmapFactory.decodeStream( is );
                if ( bmp != null )
                {
//                    bmp = scaleBgBitmap( bmp );
                    bmp = scaleBgBitmap( bmp, w, h );
                }
            }
            finally
            {
                try
                {
                    is.close();
                }
                catch( Exception e )
                {
                }
            }
        }
        return bmp;
    }

    public static Bitmap scaleBgBitmap( Bitmap b, int w, int h )
    {
        int bw = b.getWidth();
        int bh = b.getHeight();
        double s = ( double )h / ( double )bh;
        int newW = ( int )( bw * s );
        if ( newW < w )
        {
            newW = w;
        }
        return Bitmap.createScaledBitmap( b, newW, h, false );
    }


    public final static int clamp( int value, int min, int max )
    {
        int result = value;
        if ( min == max )
        {
            if ( value != min )
            {
                result = min;
            }
        }
        else if ( min < max )
        {
            if ( value < min )
            {
                result = min;
            }
            else if ( value > max )
            {
                result = max;
            }
        }
        else
        {
            result = clamp( value, max, min );
        }
        return result;
    }

}
