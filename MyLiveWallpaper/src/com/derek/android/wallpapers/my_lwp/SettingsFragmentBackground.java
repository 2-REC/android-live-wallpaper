package com.derek.android.wallpapers.my_lwp;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.util.Log;


public class SettingsFragmentBackground extends PreferenceFragment
                                        implements OnPreferenceClickListener
{
    private static final int CHOOSE_IMAGE_REQUEST  = 1;
    private static final String IMAGE_MIME_TYPE = "image/*";


    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        getPreferenceManager().setSharedPreferencesMode( Context.MODE_PRIVATE );
        getPreferenceManager().setSharedPreferencesName( LiveWallpaperImpl.SHARED_PREFS_NAME );

        addPreferencesFromResource( R.xml.prefs_background );

        Preference backgroundImage = getPreferenceManager().findPreference( SettingsActivity.PREF_BG_IMAGE );
        if ( backgroundImage != null )
        {
            backgroundImage.setOnPreferenceClickListener( this );
        }
    }

    @Override
    public boolean onPreferenceClick( Preference pref )
    {
        Intent intent = new Intent( Intent.ACTION_GET_CONTENT );
        int requestCode = 0;
        if ( pref.getKey().equals( SettingsActivity.PREF_BG_IMAGE ) )
        {
            intent.setType( IMAGE_MIME_TYPE );
            requestCode = CHOOSE_IMAGE_REQUEST;
        }
        else
        {
            return false;
        }

        try
        {
            startActivityForResult( intent, requestCode );
        }
        catch ( ActivityNotFoundException e )
        {
            Log.w( "SettingsFragmentBackground", e.getMessage() );
        }
        return true;
    }

    public void onActivityResult( int requestCode, int resultCode, Intent data )
    {
        if ( ( resultCode == Activity.RESULT_OK ) && ( requestCode == CHOOSE_IMAGE_REQUEST ) && ( data != null ) )
        {
//            String imagePath = Utils.uriToFilePath( getActivity().getBaseContext(), data.toUri( 0 ) );
            String imagePath = Utils.uriToFilePath( getActivity().getBaseContext(), data.getData() );
            if ( imagePath != null )
            {
                findPreference( SettingsActivity.PREF_BG_IMAGE ).getEditor()
                                                                .putString( SettingsActivity.PREF_BG_IMAGE, imagePath )
                                                                .commit();
            }
        }
    }

}
