package com.derek.android.wallpapers.my_lwp;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;


public class PreferenceSlider extends Preference
                              implements OnSeekBarChangeListener, View.OnKeyListener
{
    private final static int MAX_SLIDER_VALUE = 100;
    private final static int INITIAL_VALUE = 50;

    private int value = INITIAL_VALUE;
    private String minText;
    private String maxText;

    private SeekBar bar;


    public PreferenceSlider( Context context )
    {
        super( context );
        setWidgetLayoutResource( R.layout.preference_slider );
    }

    public PreferenceSlider( Context context, AttributeSet attrs )
    {
        this( context, attrs, android.R.attr.preferenceStyle );
        setWidgetLayoutResource( R.layout.preference_slider );
    }

    public PreferenceSlider( Context context, AttributeSet attrs, int defStyle )
    {
        super( context, attrs, defStyle );

        TypedArray a = context.obtainStyledAttributes( attrs, R.styleable.preference_slider, defStyle, 0 );

        minText = a.getString( R.styleable.preference_slider_minText );
        maxText = a.getString( R.styleable.preference_slider_maxText );

        a.recycle();

        setWidgetLayoutResource( R.layout.preference_slider );
    }

    @Override
    protected void onBindView( View view )
    {
        super.onBindView( view );

        if ( minText != null )
        {
            TextView minTextView = ( TextView )view.findViewById( R.id.min );
            minTextView.setText( minText );
        }

        if ( maxText != null )
        {
            TextView maxTextView = ( TextView )view.findViewById( R.id.max );
            maxTextView.setText( maxText );
        }

        bar = ( SeekBar )view.findViewById( R.id.slider );
        bar.setMax( MAX_SLIDER_VALUE );
        bar.setProgress( value );
        bar.setOnSeekBarChangeListener( this );

        view.setOnKeyListener( this );
    }

    public boolean onKey( View v, int keyCode, KeyEvent event )
    {
        if ( event.getAction() == KeyEvent.ACTION_DOWN )
        {
            if ( bar != null )
            {
                if ( ( keyCode == KeyEvent.KEYCODE_DPAD_LEFT ) && ( value > 0 ) )
                {
                    --value;
                }
                else if ( ( keyCode == KeyEvent.KEYCODE_DPAD_RIGHT ) && ( value < MAX_SLIDER_VALUE ) )
                {
                    ++value;
                }
                else
                {
                	return false;
                }

                bar.setProgress( value );
                return true;
            }
        }
        return false;
    }

    public void onProgressChanged( SeekBar seekBar, int progress, boolean fromUser )
    {
        if ( fromUser )
        {
            value = progress;
            persistInt( value );
        }
    }

    public void onStartTrackingTouch( SeekBar seekBar )
    {
    }

    public void onStopTrackingTouch( SeekBar seekBar )
    {
    }


    @Override 
    protected Object onGetDefaultValue( TypedArray ta, int index )
    {
        int dValue = ( int )ta.getInt( index, INITIAL_VALUE );

        return ( int )Utils.clamp( dValue, 0, MAX_SLIDER_VALUE );
    }


    @Override
    protected void onSetInitialValue( boolean restoreValue, Object defaultValue )
    {
        value = defaultValue != null ? ( Integer )defaultValue : INITIAL_VALUE;

        if ( !restoreValue )
        {
            persistInt( value );
        }
        else
        {
            value = getPersistedInt( value );
        }
    }

}
