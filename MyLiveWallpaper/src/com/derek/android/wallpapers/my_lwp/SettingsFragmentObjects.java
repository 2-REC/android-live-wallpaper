package com.derek.android.wallpapers.my_lwp;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceFragment;


public class SettingsFragmentObjects extends PreferenceFragment
{
    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        getPreferenceManager().setSharedPreferencesMode( Context.MODE_PRIVATE );
        getPreferenceManager().setSharedPreferencesName( LiveWallpaperImpl.SHARED_PREFS_NAME );

        addPreferencesFromResource( R.xml.prefs_objects );
    }

}
